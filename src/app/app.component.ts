import {Component, OnInit, ViewChild} from '@angular/core';
import {ResolverCircuit} from "./resolver-circuit.service";
import {NgForm} from "@angular/forms";
import {MapService} from "./map/map.service";
import {CitiesService} from "./cities-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLoading = false;
  errorMessage = null;

  constructor(private resolverCircuit: ResolverCircuit, private mapService: MapService, private citiesService: CitiesService) {
  }

  cities = [
    "Clermont-Ferrend",
    "Bordeaux", "Bayonne",
    "Toulouse", "Marseille",
    "Nice", "Nantes",
    "Rennes", "Paris",
    "Lille", "Dijon",
    "Valence", "Aurillac",
    "Orléans", "Reims",
    "Strasbourg", "Limoges",
    "Troyes", "Le Havre",
    "Cherbourg", "Brest",
    "Niort"];

    path = []

  ngOnInit() {

  }

  onValidated(form: NgForm) {
    this.errorMessage = null;
    this.isLoading = true;
    if (form.valid) {
      this.resolverCircuit.getCircuit(form.value.cityStart).subscribe(
        (cities: []) => {
          this.citiesService.updateCities(cities);
          this.path = cities;
          this.isLoading = false;
        },
        (e) => {
          this.errorMessage = e.message
          this.isLoading = false;
        })
    }

  }
}
