import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

interface CitiesResponseData {
  cities: {};
}

@Injectable({providedIn: 'root'})
export class ResolverCircuit {
  constructor(private http: HttpClient) {
  }
  cities = [
    "Niort",
    "Bordeaux", "Bayonne",
    "Toulouse", "Marseille",
    "Nice", "Nantes",
    "Rennes", "Paris",
    "Lille", "Dijon",
    "Valence", "Aurillac",
    "Orléans", "Reims",
    "Strasbourg", "Limoges",
    "Troyes", "Le Havre",
    "Cherbourg", "Brest",
    "clermont-ferrend"
  ];

  getCircuit(city: string) {
    return this.http.post<any>('http://127.0.0.1:5000/city', {
      city
    });
  }
}
