import { Injectable} from "@angular/core";
import {Subject} from "rxjs";

@Injectable({providedIn: 'root'})
export class CitiesService {
  cities = new Subject<[]>();

  updateCities(cities: []) {
    this.cities.next(cities);
  }
}
