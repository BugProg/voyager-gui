import {Component, OnInit, ChangeDetectorRef} from "@angular/core";
import * as Mapboxgl from 'mapbox-gl';
import {environment} from "../../environments/environment";
import {MapService} from "./map.service";
import {ResolverCircuit} from "../resolver-circuit.service";
import {CityModel} from "./city.model";
import {CitiesService} from "../cities-service";

@Component({
  selector: 'map-app',
  templateUrl: './map.component.html',
  styleUrls: ['']
})
export class MapComponent implements OnInit {
  map: any;

  citiesCoordinates: CityModel[] = [
    new CityModel('clermont-ferrend', [3.08194, 45.77746]),
    new CityModel('bordeaux', [-0.5699, 44.8427]),
    new CityModel('bayonne', [-1.4752, 43.4934]),
    new CityModel('toulouse', [1.4299, 43.6023]),
    new CityModel('marseille', [5.3792, 43.3017]),
    new CityModel('nice', [7.2771, 43.7121]),
    new CityModel('nantes', [-1.5542, 47.2182]),
    new CityModel('rennes', [-1.6788, 48.1136]),
    new CityModel('paris', [2.3482, 48.8577]),
    new CityModel('lille', [3.0707, 50.6355]),
    new CityModel('dijon', [5.0331, 47.3211]),
    new CityModel('valence', [4.893493, 44.933277]),
    new CityModel('aurillac', [2.4445, 44.9286]),
    new CityModel('orleans', [1.9089, 47.9016]),
    new CityModel('reims', [4.0313, 49.2562]),
    new CityModel('strasbourg', [7.7454, 48.5832]),
    new CityModel('limoges', [1.2675, 45.8321]),
    new CityModel('troyes', [4.0711, 48.2988]),
    new CityModel('le-havre', [0.1099, 49.4931]),
    new CityModel('cherbourg', [-1.6213, 49.6418]),
    new CityModel('brest', [-4.4824, 48.3927]),
    new CityModel('niort', [-0.4623, 46.3250])
  ]

  citiesPath: string [] = []

  constructor(private mapService: MapService, private CircuitService: ResolverCircuit, private citiesService: CitiesService) {
  }

  ngOnInit() {

    this.citiesService.cities.subscribe((newCities: string[]) => {
      (Mapboxgl as any).accessToken = environment.token;
      this.map = new Mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [2.61878695312962, 47.8249046208979],
        zoom: 6
      });
      this.citiesPath = newCities;

      let cities = []
      for (let i in this.citiesPath) {
        {
          cities.push(
            {
              'type': 'Feature',
              'geometry': {
                'type': 'Point',
                'coordinates': this.getCoordinate(this.citiesPath[i].toLowerCase())
              },
              'properties': {
                'title': i + ' - ' + this.citiesPath[i].toLowerCase()
              }
            },
          )
        }
      }
      console.log("JSON :", cities)
      this.mapService.addPosition(this.map, cities);
    });
  }

  getCoordinate(name: string) {
    const city = this.citiesCoordinates.find(
      (s) => {
        return s.name === name;
      }
    );
    return city == null ? 'error' : city.coordinate;
  }
}
