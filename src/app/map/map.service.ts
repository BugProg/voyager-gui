import {Injectable} from "@angular/core";
import {CityModel} from "./city.model";

@Injectable({providedIn: 'root'})
export class MapService {

  addPosition(map: any, cities: any) {
    let coordinates: Array<any> = []
    console.log('CITIES :', cities[0].geometry.coordinates)
    for (let i in cities) {
      console.log(cities[i].geometry.coordinates)
      coordinates.push(cities[i].geometry.coordinates)
    }
    console.log(coordinates)

    map.on('load', () => {
// Add an image to use as a custom marker
      map.loadImage(
        'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
        (error: any, image: any) => {
          if (error) throw error;
          map.addImage('custom-marker', image);
// Add a GeoJSON source with 2 points
          map.addSource('points', {
            'type': 'geojson',
            'data': {
              'type': 'FeatureCollection',
              'features': cities
            }
          });

// Add a symbol layer
          map.addLayer({
            'id': 'points',
            'type': 'symbol',
            'source': 'points',
            'layout': {
              'icon-image': 'custom-marker',
// get the title name from the source's "title" property
              'text-field': ['get', 'title'],
              'text-font': [
                'Open Sans Semibold',
                'Arial Unicode MS Bold'
              ],
              'text-offset': [0, 1.25],
              'text-anchor': 'top'
            }
          });
          map.addSource('route', {
            'type': 'geojson',
            'data': {
              'type': 'Feature',
              'properties': {},
              'geometry': {
                'type': 'LineString',
                'coordinates': coordinates
              }
            }
          });
          map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
              'line-join': 'round',
              'line-cap': 'round'
            },
            'paint': {
              'line-color': '#000',
              'line-width': 3
            }
          });
        }
      );
    });
  }
}



