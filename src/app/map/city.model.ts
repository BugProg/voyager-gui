export class CityModel {
  public name: string;
  public coordinate: [long: number, lat: number]

  constructor(
    name: string,
    coordinate: [long: number, lat: number]
  ) {
    this.name = name;
    this.coordinate = coordinate
  }
}
